package hu.braininghub.bh06;

import java.io.IOException;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class MessagingServlet
 */
@WebServlet("/MessagingServlet")
public class MessagingServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    @Inject
    MyReceiver myReceiver;
    
    @Inject
    MySender mySender;
	
	
    public MessagingServlet() {
        super();
        
    }

	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String message = request.getParameter("message");
	
		mySender.addsender(message);
		myReceiver.addreceiver();
		
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
	}

}
