package hu.braininghub.bh06;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

public class MyListener implements MessageListener {

	@Override
	public void onMessage(Message m) {
		try {
			TextMessage message = (TextMessage) m;
			System.out.println("following message is received:" + message.getText());
		} catch (JMSException e) {
			System.out.println(e);
		}

	}


}
