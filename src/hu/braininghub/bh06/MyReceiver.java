package hu.braininghub.bh06;

import javax.ejb.Stateless;
import javax.jms.Queue;
import javax.jms.QueueConnection;
import javax.jms.QueueConnectionFactory;
import javax.jms.QueueReceiver;
import javax.jms.QueueSession;
import javax.jms.Session;
import javax.naming.InitialContext;

@Stateless
public class MyReceiver {
	public void addreceiver(){
		try {
			InitialContext ctx = new InitialContext();
			QueueConnectionFactory factory = (QueueConnectionFactory) ctx.lookup("myQueueConnectionFactory");
			QueueConnection con = factory.createQueueConnection();
			con.start();

			QueueSession session = con.createQueueSession(false, Session.AUTO_ACKNOWLEDGE);
			Queue t = (Queue) ctx.lookup("myQueue");
			QueueReceiver receiver = session.createReceiver(t);

			MyListener listener = new MyListener();
			receiver.setMessageListener(listener);

			System.out.println("Receiver1 is ready, waiting for messages...");
			System.out.println("press Ctrl+c to shutdown...");

			while (true) {
				Thread.sleep(1000);
			}
		} catch (Exception e) {
			System.out.println(e);
		}
	}

}
