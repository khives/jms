package hu.braininghub.bh06;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import javax.ejb.Stateless;
import javax.jms.Queue;
import javax.jms.QueueConnection;
import javax.jms.QueueConnectionFactory;
import javax.jms.QueueSender;
import javax.jms.QueueSession;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.naming.InitialContext;

@Stateless
public class MySender {

	public void addsender(String s) {
		try {
			InitialContext ctx= new InitialContext();
			QueueConnectionFactory factory= (QueueConnectionFactory)ctx.lookup("myQueueConnectionFactory");
			QueueConnection con=factory.createQueueConnection();
			con.start();
			
			QueueSession session= con.createQueueSession(false, Session.AUTO_ACKNOWLEDGE);
			
			Queue t= (Queue) ctx.lookup("myQueue");
			QueueSender sender=session.createSender(t);
			TextMessage msg=session.createTextMessage();
			//BufferedReader b= new BufferedReader(new InputStreamReader(System.in));
			/*while(true) {
				System.out.println("Enter message, end to terminate");
				String s= b.readLine();
				if(s.equals("end"))
					break;
				msg.setText(s);
				sender.send(msg);
				System.out.println("Message successfully sent.");
			}*/
			
			msg.setText(s);
			sender.send(msg);
			
			
			con.close();
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
		
	}
}
